import { config } from 'dotenv';
config();

import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';

import { AppModule } from './app/app.module';
import { urlencoded, json } from 'express';
import { HttpsOptions } from '@nestjs/common/interfaces/external/https-options.interface';
import { readFileSync } from 'fs';

async function bootstrap() {
	// Load SSL certificate and key
    const httpsOptions: HttpsOptions = {
        key: readFileSync(process.env.HTTPS_KEY_PATH, 'utf8'),
        cert: readFileSync(process.env.HTTPS_CERT_PATH, 'utf8'),//still need to fix this
    };

    // Include httpsOptions in create call
    const app = await NestFactory.create(AppModule, { cors: true, httpsOptions });
	const globalPrefix = 'api';

	const port = process.env.PORT || 3333;

	app.setGlobalPrefix(globalPrefix);
	app.use(json({ limit: '50mb' }));
	app.use(urlencoded({ extended: true, limit: '50mb' }));
	await app.listen(port);

	Logger.log(
		`🚀 ${process.env.PROJECT_NAME
		} API is running on: ${await app.getUrl()}`,
	);
}
bootstrap();
